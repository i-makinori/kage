



function check_submit(){
  if(window.confirm("入力された内容を変更として適用するよう、リクエストを投稿します"))
  {return true;}else{return false;}
}



function show_or_hide_password() {
  var x = document.getElementById("password-input");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function getFirstElementByClassName(_class){
  return getElementsByClassName(_class)[0];
}

function documents_table_select_document(id) {
  var row_value_old = document.getElementById(id);
  var metadata_form = document.getElementById("forming-metadata");


  var pathname_rows_old = row_value_old.getElementsByClassName("pathname")[0];
  var pathname_form_old = document.getElementById("forming-metadata-old-pathname");
  var pathname_form_new = document.getElementById("forming-metadata-new-pathname");
  pathname_form_old.value = pathname_rows_old.innerHTML;
  pathname_form_new.value = pathname_rows_old.innerHTML;

  var old_attributes = row_value_old.getElementsByClassName("attributes")[0];
  var new_attributes = document.getElementById("forming-metadata-new-attributes");
  new_attributes.value = old_attributes.innerHTML;
  
  var title_old = row_value_old.getElementsByClassName("title")[0];
  var title_new = metadata_form.getElementsByClassName("title")[0];
  title_new.firstElementChild.innerText = title_old.innerHTML;

  var caption_old = row_value_old.getElementsByClassName("caption")[0];
  var caption_new = metadata_form.getElementsByClassName("caption")[0];
  caption_new.firstElementChild.innerText = caption_old.innerHTML;

  alert("メタ情報の編集をします。");
  
}
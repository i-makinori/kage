(in-package #:kage)

;;; html markup



(defparameter *parsed-tml* nil)

(defun concatstrs (&rest strings)
  (apply #'concatenate 'string strings))


(defun format-tml-content-escaped (content-string)
  (let ((escape-list 
          '(("&" . "&amp;") ("<" . "&lt;") (">" . "&gt;") #| (" " . "&nbsp;") |#
            ("©" . "&copy;") ("'" . "&#39;") ("\"" . "&quot;") ("¥" . "&yen;")))
        (current-string (copy-seq content-string ) ))
  (reduce #'(lambda (from-to from-to-list)
              (setf current-string
                    (cl-ppcre:regex-replace-all (car from-to) current-string (cdr from-to)))
              from-to-list)
          escape-list)
    current-string))


(defun parse-markup-s-attribute (tag-list format-string)
  (cond ((and (keywordp (car tag-list)) 
              (cadr tag-list))
         (parse-markup-s-attribute
          (cddr tag-list)
          (concatstrs format-string
                      (format nil " ~(~a~)=\"~A\"" (car tag-list) (cadr tag-list)))))
        (t 
         ;; (format t "tag-list-attr : ~A~%" tag-list)
         (cons tag-list format-string))))

(defun whitespaces (num)
  (format nil "~v@{~A~:*~}" num " "))


(defun parse-markup (tag-list &key (after-attribute nil) (nest 0))
  "format xtml from key as taged tree"
  ;; (format t "~%after-attribute, tag-list: ~A  ~% ~A ~%" after-attribute tag-list)

  (cond 
    ((null tag-list)
     "")
    ((not (listp tag-list))
     (format-tml-content-escaped (format nil "~A" tag-list)))

    ((listp (car tag-list))
     (concatstrs (parse-markup (car tag-list) :nest nest)
                 (parse-markup (cdr tag-list) :after-attribute after-attribute :nest nest)))
    ((eq (car tag-list) :markdown)
     (concatstrs
      (format nil "~A~A~%" (whitespaces 0) (cadr tag-list))
      (parse-markup (cddr tag-list) :nest nest)))
    ((and (keywordp (car tag-list))
          (not after-attribute)
          )
     (let ((attribute (parse-markup-s-attribute (cdr tag-list) "")))
       (concatstrs
        (format nil "~%~A<~(~a~)~A"
                (whitespaces 0)
                (car tag-list)
                (cdr attribute))
        (if (not (car attribute))
            (format nil " />")
            (format nil ">~A</~(~a~)>"
                    (parse-markup (car attribute) :after-attribute t :nest (+ nest 2))
                    (car tag-list))))))

    ((and after-attribute (keywordp (car tag-list)))
     (concatstrs (parse-markup (car tag-list) :after-attribute after-attribute :nest nest)
                 (parse-markup (cdr tag-list) :after-attribute after-attribute :nest nest)))

    ((not (listp (car tag-list)))
     (concatstrs (parse-markup (car tag-list) :after-attribute after-attribute :nest nest)
                 (parse-markup (cdr tag-list) :after-attribute after-attribute :nest nest)))
    
    (t
     ;;(warning "html tag-list error")
     tag-list)
    ))



(defun add-doctype-html (html-string &key (doctype nil))
  (let ((doctype-string
          (case doctype
            (:html "<!DOCTYPE html>")
            (:xhtml "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">")
            (t ""))))
  (format nil "~A~%~A" doctype-string html-string)))


(defun parse-as-html (tag-list) 
  (add-doctype-html (format nil (format nil "~A" (parse-markup tag-list :nest 0)))
                    :doctype :xhtml))



;;; test



(defparameter *html-list-sample*
  '(:html :lang "ja"
  ;;`(:html :xmlns "http://www.w3.org/1999/xhtml" :|xml:lang| "ja" :lang "ja"
    (:head
     (:title "hogehoge")
     (:link :href "lambda" :rel "hoge/fuga")
     (:link :href "cons" :rel "foo/bar"))
    (:body
     (:div :id "the-id" :class "classed"
      (:ul
       (:li (:a :href "/INDEX" "index"))
       (:li (:a :href "/meta-list" "pages-list"))
       (:li (:a :href "/statics" "static-files"))
       (:li (:a :href "/documents" "documents" (:b "...the documens!!!")))
       (:li (:a :href "/editor" "editor" "the" "editor" :aaa "is"))
       (:li (:a :href "/editor" "editor" "the" "editor" :aaa "out tag" 
             (:b "b on tag") "uuu out tag" ))))
     (:hr)
     (:p '(1 2 3 4 5))
     (:p "escape characters: <aaa>&- &lt;"))))


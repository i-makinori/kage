(in-package #:kage)

;; template

(defparameter *html-declaration*
  `(:xmlns "http://www.w3.org/1999/xhtml" :|xml:lang| "ja" :lang "ja"))

(defparameter *tokinowari-head*
  (lambda (&key (title ""))
    `(:head
      (:meta :http-equiv "Content-Type" :content "text/html; charset=UTF-8")
      (:title ,title "- n次元情報空間因果律基底研究室幻影保管庫")
      (:link :rel "icon" :href "/favicon.ico")
      (:link :rel "stylesheet" :href "/statics/css/normalize.css")
      (:link :rel "stylesheet" :href "/statics/css/style.css")
      (:script :type "text/javascript"
       :src "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_CHTML"
       ())
      (:script :type "text/javascript"
       :src "/statics/javascript/utils.js"
       ())
)))

(defparameter *tokinowari-header*
  (lambda (&key (title ""))
    (format nil "~A - n次元情報空間因果律基底研究室幻影保管庫 -" title )))

(defparameter *tokinowari-side-menu*
  `((:ul
     (:li (:a :href "/" "入り口"))
     (:li (:a :href "/meta-list" "kura以下のファイル一覧?"))
     (:li (:a :href "/metas" "kuraの管理下のドキュメント"))
     (:li (:a :href "/statics" "落とし込み"))
     (:li "書記室"
      (:ul
       (:li (:a :href "/librarian" "入室"))
       (:li (:a :href "/librarian/edit-document" "文書毎への内容の編集"))
       (:li (:a :href "/librarian/metas" "書庫の管理、目録の管理"))))
     (:li "運用試験"
      (:ul
       (:li (:a :href "/hunchentoot/test" "hunchentoot test"))
       (:li (:a :href "/markdown-test" "markdown test")))))
    ))

(defparameter *tokinowari-footer*
  `("池上 蒔典 <maau3p@gmail.com>"))

(defparameter *tokinowari-body*
  (lambda (&key (title "") (content "") (container-class "") (show-space? nil))
    `(:body
      (:div :class "screen-container"
       (:div :class ,container-class
        (:div :class "header"
         ,(funcall *tokinowari-header* :title title))
        (:hr :class "invisible")
        (:div :class "side-menu"
         ,*tokinowari-side-menu*)
        (:hr :class "invisible")
        (:div :class "article-main"
         ,(funcall content))
        (:hr :class "invisible")
        (:div :class "navi-menu" nil)
        ,@(if show-space?
              `((:div :class "space-menu" nil)
                ))
        (:hr :class "invisible")
        (:div :class "footer"
         ,*tokinowari-footer*)
        (:hr :class "invisible"))))))


(defparameter *tokinowari-template*
  (lambda (&key (title "") (content ""))
    content
    (parse-as-html
     `(:html ,@*html-declaration*
       ,(funcall *tokinowari-head* :title title)
       ;;
       ,(funcall *tokinowari-body*
                 :title title :content content :container-class "grid-container"
                 )))))

(defparameter *tokinowari-template-wide-article*
  (lambda (&key (title "") (content ""))
    content
    (parse-as-html
     `(:html ,@*html-declaration*
       ,(funcall *tokinowari-head* :title title)
       ;;
       ,(funcall *tokinowari-body*
                 :title title :content content
                 :container-class "grid-container-wide-article"
                 :show-space? t)))))


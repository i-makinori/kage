(in-package #:kage)

;;; parse markdown


#|
(defun markdown-file-to-html-string (path)
  (multiple-value-bind (x str)
      (cl-markdown:markdown (uiop:read-file-string path)
                            :stream nil)
    x str))
 

(defun markdown-string-to-html-string (markdown-string)
  (multiple-value-bind (x str)
      (cl-markdown:markdown markdown-string
                            :stream nil)
    x str))

|#

;; by Pandoc, shell command



;;(ql:quickload :inferior-shell)

(defun run-pandoc-generate-html-string (path)
  (inferior-shell:run/ss
   `(pandoc --table-of-contents --mathjax -f markdown -t html ,path)))

(defun markdown-file-to-html-string (path)
  (format nil "~A"
          (run-pandoc-generate-html-string path)))

(defun markdown-string-to-html-string (markdown-string)
  (uiop:with-temporary-file (:stream s :pathname p :keep nil)
    s
    (progn
      (write-file markdown-string p :action-if-exists :supersede)
      (run-pandoc-generate-html-string p))))

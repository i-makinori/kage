(in-package #:kage)

;;; macro

(defmacro define-handler (description lambda-list &body body)
  `(hunchentoot:define-easy-handler ,description ,lambda-list ,@body))

;;; handlers 

(defparameter *documents-dir-uri*
  "/documents/")

(defparameter *raw-file-dif-uri*
  "/raw/")

(defun default-tokinoari-dispatch-table ()
 (nconc
  (list 'hunchentoot:dispatch-easy-handlers
        (hunchentoot:create-static-file-dispatcher-and-handler
         "/favicon.ico"
         (make-pathname :name "favicon" :type "ico" :version nil
                        :defaults *static-files-directory-pathname*))
        (hunchentoot:create-folder-dispatcher-and-handler
         "/statics/"
         (make-pathname :name nil :type nil :version nil
                        :defaults *static-files-directory-pathname*)
         nil)
        (create-documents-dispatcher-and-handler
         *documents-dir-uri* *kura-documents-bases-dir*)
        (hunchentoot:create-folder-dispatcher-and-handler
         *raw-file-dif-uri*
         (make-pathname :name nil :type nil :version nil
                        :defaults *kura-documents-bases-dir*))
        )
  (mapcar
   (lambda (args)
     (apply 'hunchentoot:create-prefix-dispatcher args))
   '())))




;; index

(define-handler (index :uri "/") ()
  (setf (hunchentoot:content-type*) "text/html")
  (funcall *tokinowari-template*
           :title "総覧"
           :content 
           (lambda ()
             `(:div :class "article"
               (:p "主要とした文書の目録"
                "universal-time: " ,(get-universal-time))))))

(define-handler (index-dedir :uri "/index") ()
  (hunchentoot:redirect "/"))


(define-handler (markdown-test :uri "/markdown-test") ()
  (setf (hunchentoot:content-type*) "text/html")
  (tokinowari-document-page
   :title "markdownをドメインの枠組みを適用したhtmlへと射影する試験"
   :markdown (uiop:read-file-string *sample-markdown-file-path*)))


(defun tokinowari-document-page (&key (title "") (date 0) (attributes nil) (markdown nil))
  (funcall *tokinowari-template*
           :title title
           :content
           (lambda ()
             (tokinowari-document-markdown-div
              :title title :date date :attributes attributes
              :markdown markdown))))

(defun tokinowari-document-markdown-div (&key (title "") (date nil) (attributes nil) (markdown nil))
  (print markdown)
  `(:div :class "article"
    (:h1 ,title)
    (:p "最終更新 : " ,(datetime-to-datetime-string date))
    (:p "属性 : " ,attributes)
    ,(if (stringp markdown)
         `(:markdown ,(markdown-string-to-html-string markdown))
         (list "markdownの文字列が存在しません。"))))


;; documents-list

(define-handler (pages-list :uri "/meta-list") ()
  (setf (hunchentoot:content-type*) "text/html")
  (let* ((files-in-kura-dir
           (remove-if #'cl-fad:directory-pathname-p
                      (cl-fad:list-directory *kura-documents-bases-dir*)))
         (files-table
           `(:table
             (:tr (:th "filename") (:th "uri"))
             ,@(mapcar 
                #'(lambda (path)
                    `(:tr
                      (:td ,(file-namestring path))
                      (:td (:a :href
                            ,(merge-pathnames "/documents/" (file-namestring path))
                            ,(file-namestring path)))))
                    files-in-kura-dir))))
    
    (funcall *tokinowari-template*
             :title "記事総覧"
             :content 
             (lambda ()
               `(:div :class "article"
                 (:p "kura directory直下のファイル一覧、内容物としての表示での行き先")
                  ,files-table)))))


;; documents

(define-handler (documents :uri "/documents") ()
  (setf (hunchentoot:content-type*) "text/html")
  (hunchentoot:redirect "/meta-list"))

(defun return-document-as-content (file-pathname &key (title ""))
  (setf (hunchentoot:content-type*) "text/html")
  (cond ((and (cl-fad:file-exists-p file-pathname)
              (string= (pathname-type file-pathname) "md"))
         (funcall *tokinowari-template*
                  :title title
                  :content
                  (lambda ()
                    `(:div :class "article"
                      (:markdown ,(markdown-file-to-html-string file-pathname))))))
        ((cl-fad:file-exists-p file-pathname)
         (page-405))
        (t
         (page-404))))

(defun create-documents-dispatcher-and-handler (uri-prefix base-path)
  (flet
      ((handler ()
         (let ((request-path (hunchentoot:request-pathname hunchentoot:*request* uri-prefix))
               (file-path (cl-fad:file-exists-p 
                           (cl-fad:merge-pathnames-as-file base-path
                                                           (file-namestring
                                                            (hunchentoot:script-name*))))))
           (cond ((null request-path)
                  (page-404))
                 ((null file-path)
                  (page-404))
                 (t 
                  (return-document-as-content file-path))))))
    (hunchentoot:create-prefix-dispatcher uri-prefix #'handler)))



;;; documents metadata-tables


(defun template-documents-table-row (pathname edit mimetypes attributes href raw
                                     title dates author caption olds opend-to
                                     &key (form? nil) (apply nil))
  `(:span :class ,(if form? "documents-table-row-form" "documents-table-row")
    (:span :class "pathname" ,pathname)
    (:span :class "href"  ,href) (:span :class "raw" ,raw)
    (:span :class "mimetypes" ,mimetypes) (:span :class "attributes" ,attributes)
    (:span :class "title" ,title)
    (:span :class "dates" ,dates) (:span :class "olds" ,olds)
    (:span :class "caption" ,caption)
    (:span :class "author" ,author) (:span :class "opend-to" ,opend-to)
    ,(if form? `(:span :class "apply" ,apply))
    ,(if form? `(:span :class "edit" ,edit))
    (:hr :class "invisible")))



(defun href-of-document (relative-pathname-from-kura &optional (text ""))
  `(:a :href ,(merge-pathnames *documents-dir-uri* relative-pathname-from-kura)
    ,text))

(defun href-of-raw-file (relative-pathname-from-kura &optional (text ""))
  `(:a :href ,(merge-pathnames *raw-file-dif-uri* relative-pathname-from-kura)
    ,text))

(defun documents-table-for-visitors ()
  (labels ((document-row (doc) ;; document
             (template-documents-table-row
              (document-path doc)
              `(:a :href "" "edit")
              (document-path doc)
              (document-attributes doc) 
              (href-of-document (document-path doc) "href")
              (href-of-raw-file (document-path doc) "raw")
              (document-title doc)
              (datetime-to-datetime-string (document-dates doc)) (document-author doc)
              (document-caption doc)
              (document-olds doc)
              "all")))
  (let* ((table-head
           (template-documents-table-row
            "pathname" "edit" "mimetypes" "attributes" "href" "raw"
            "title" "dates" "author" "caption" "olds" "opend to"))
         (documents-list *kura-documents*)
         (document-rows
           (mapcar #'document-row documents-list)))
    `(:div :class "documents-table"
      (,table-head
       ,@document-rows)))))

(define-handler (metas :uri "/metas") ()
  (read-kura-bases-json-file-to-structures!)
  (let* ((documents-table
           (documents-table-for-visitors)))
    (funcall
     *tokinowari-template*
     :title "目録の管理"
     :content
     #'(lambda ()
       `(:div :class "article"
         (:h3 "記事の一覧")
         ,documents-table)))))



;;; status code not are accept

(defun page-404 ()
  (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)  
  (funcall
   *tokinowari-template*
   :title "404 - not found"
   :content
   (lambda ()
     `(:div :class "article"
       (:h1 "404 - not found")
       (:p "request に対応する、"
        (:a :href ,(hunchentoot:request-uri*) ,(hunchentoot:request-uri*))
        "というuriに対応するページは、存在しません"
        )))))

(defmethod hunchentoot:acceptor-status-message
    (acceptor (http-status-code (eql 404)) &key)
  (page-404))


(defun page-405 ()
  (setf (hunchentoot:return-code*) hunchentoot:+http-method-not-allowed+)  
  (funcall
   *tokinowari-template*
   :title "405 - method not allowed"
   :content
   (lambda ()
     `(:div :class "article"
       (:h1 "405 - method not allowed")
       (:p (:a :href ,(hunchentoot:request-uri*) ,(hunchentoot:request-uri*))
        "に対して、詳細の開示を許可していません。"
        )
       (:p
        ,(href-of-raw-file (file-namestring (hunchentoot:script-name*)) "raw")
        "からの接続も試してみて下さい。")))))

(defmethod hunchentoot:acceptor-status-message
    (acceptor (http-status-code (eql 405)) &key)
  (page-405))


(in-package #:kage)

;;; routes for librarians

;; auth

(defun accessiond? ()
  (if (and (equal (hunchentoot:session-value 'username) "iruka-user")
           (equal (hunchentoot:session-value 'password) "dolphins"))
      (hunchentoot:session-value 'username) ;; user's data
      nil))

(defun auth-update-username-and-password-session-by-post-data ()
  (let ((new-username (hunchentoot:post-parameter "new-username")))
    (when new-username
      (setf (hunchentoot:session-value 'username) new-username)))
  (let ((new-password (hunchentoot:post-parameter "new-password")))
    (when new-password
      (setf (hunchentoot:session-value 'password) new-password))))

(defun auth-article (accessiond)
  `(:div :class "article"
    ,@(if (and (not accessiond)
               (eq :post (hunchentoot:request-method*)))
          `((:h3 "照合に失敗しました")
            (:p "postに対する、username:"
             (:code ,(hunchentoot:post-parameter "new-username"))
             "の照合は失敗しました。")
            (:hr)))
    ,@(if accessiond
          `((:h3 "照合され、入室しています")
            (:p "おかえりなさい、" (:code ,accessiond) "。あなたは編集をしています。")
            (:hr)))
    (:h3 ,(if accessiond "再照合" "照合"))
    (:p "個人を照合します")
    (:form :method :post
     (:p "名前(usename): " (:input :type "text" :name "new-username"
                          :value ,(or (hunchentoot:session-value 'username) "")))
     (:p "パスワード: "
      (:input :type "password" :name "new-password" :id "password-input"
       :value ,(or (hunchentoot:session-value 'password) ""))
      (:input :type "checkbox" :onclick "show_or_hide_password()") " 隠す/表す")
     (:input :type :submit :value "入室"))))

(defun auth-page ()
  (auth-update-username-and-password-session-by-post-data)
  (let ((accessiond (accessiond?)))
           (funcall
            *tokinowari-template*
            :title (if accessiond "入室しています。" "入室します。")
            :content
            #'(lambda () (auth-article accessiond)))))

(defmacro define-handler-of-librarians (description lambda-list &body when-librarian)
  `(define-handler ,description ,lambda-list
     (setf (hunchentoot:content-type*) "text/html")
     (read-kura-bases-json-file-to-structures!)
     (auth-update-username-and-password-session-by-post-data)
     (cond 
       ((accessiond?) ,@when-librarian)
       (t (auth-page)))))

(define-handler-of-librarians (librarian-page :uri "/librarian") ()
  (funcall *tokinowari-template*
           :title "enterd"
           :content #'(lambda () 
                        (auth-article (accessiond?)))))



;; document editor


(defun document-editor-form (document-file-name document-title attributes content
                             &optional (posted? nil))
  `((:div :class "editor"
     (:h3 "記事の編集")
     (:form :method "POST"
      ,(if document-file-name
           `(:p "ファイル名" (:input :type "text" :name "document-file-name"
                              :readonly "readonly"
                              :value ,document-file-name)
             " を編集しています")
           `(:p "ファイル名" (:input :type "text" :name "document-file-name"
                              :reduired "required"
                              :value ,(or document-file-name ""))
             " を新規に作成します"))
      (:p "記事の表題" (:input :type "text" :name "document-title"
                        :required "required" :value ,(or document-title "")))
      (:p "記事の属性" (:input :type "text" :name "attributes" :value ,(or attributes "")))
      (:p "記事の内容")
      (:textarea :name "content" :rows "13" :cols "89" :required "required" 
       ,(or content ""))
      (:p
       (:input :type "submit" :value "投稿テスト")
       ,(when posted? `(:input :type "submit" :value "投稿する")))))))

(defun document-editor-test (document-file-name document-title attributes content)
   `((:div :class "test-article"
      (:h3 "記事の投影試験")
      (:p "ファイル名" ,document-file-name)
      (:p "記事の表題" ,document-title)
      (:p "記事の属性" ,attributes)
      (:p "以下、表示される記事の内容となります。")
      (:hr)
      ,(tokinowari-document-markdown-div
        :title document-title :date (get-universal-time)
        :attributes attributes
        :markdown content))))

(define-handler-of-librarians (editor :uri "/librarian/edit-document")
    (document-file-name document-title attributes content) 
  (let* ((posted? (eq :post (hunchentoot:request-method*))))
    (funcall
     *tokinowari-template*
     :title "記事の編集。"
     :content
     (lambda ()
       `(:div :class "article"
         ,(document-editor-form document-file-name document-title attributes content
                                posted?)
         (:hr)
         ,(document-editor-test document-file-name document-title attributes content))))))

;; static files

(define-handler (file-ud-load-editor :uri "/file-udloader") ()
    (cond 
      ((accessiond?)
       (funcall
        *tokinowari-template*
        :title "静的ファイルの管理。"
        :content
        (lambda ()
          `(:div :class "article"
            (:h3 "静的ファイルの一覧")))))))


;; metadata editor

(defun href_blank (href text)
  `(:a :href ,href :target "_blank" ,text))

(defun documents-table-for-editing-table-head ()
  (template-documents-table-row
   "pathname" "記事の内容" "mimetypes" "attributes" "href" "raw"
   "title" "dates" "author" "caption" "olds" "opend to"
   :apply "メタ情報" :form? t))

(defun documents-table-for-editing ()
  (labels
      ((document-row (doc counter)
         (let ((current-row-id (format nil "metadatas-editor-~A" counter)))
           `(:div :id ,current-row-id
             ,(template-documents-table-row
               (document-path doc)
               (href_blank "" "編集")
               (document-path doc)
               (document-attributes doc)
               (href-of-document (document-path doc) "href")
               (href-of-raw-file (document-path doc) "raw")
               (document-title doc)
               (datetime-to-datetime-string (document-dates doc)) (document-author doc)
               (document-caption doc)
               (document-olds doc)
               "all"
               :form? t
               :apply `(:input :type "button" :value "▲管理"
                        :onclick
                        ,(format nil "documents_table_select_document('~A')" current-row-id)))))))
    (let* ((table-head
             (documents-table-for-editing-table-head))
           (documents-list *kura-documents*)
           (document-rows
             (loop for doc in documents-list
                   for iter from 0
                   collect (document-row doc iter))))
      document-rows
    `(:div :class "documents-table"
      (,table-head
       ,@document-rows)))))

(defun documents-table-for-forming (&key (old-pathname "") (new-pathname "")
                                         (new-title "") (new-attributes "") (new-caption ""))
  `(:div :class "documents-table"
    (,(documents-table-for-editing-table-head)
     (:form :method "post" :id "forming-metadata" :onsubmit "return check_submit()"
      (:input :type "hidden" :name "conduct" :value "forming")
      ,(template-documents-table-row
        `(:span
          (:p "old: " (:input :type "text" :name "old-pathname" :id "forming-metadata-old-pathname"
                        :value ,old-pathname ))
          (:p "new: " (:input :type "text" :name "new-pathname" :id "forming-metadata-new-pathname"
                       :value ,new-pathname)))
        (href_blank "" "編集")
        "新たなpathnameに対して、対応が付けられます。"
        `(:input :type "text" :name "new-attributes" :id "forming-metadata-new-attributes"
          :value ,new-attributes)
        "href" "raw"
        `(:textarea :name "new-title" ,new-title)
        "変更が認証された時に、 その日時となります。"
        "管理の為のリクエストの、 送信者に基づきます。"
        `(:textarea :name "new-caption" ,new-caption)
        "変更が認証された時に、 変更されます。"
        "未実装です"
        :form? t
        :apply `(:input :type "submit" :value "適用"))))))

(defun nil->nil-string (S)
  (if (null S) "" S))

(defun string-made-of (string characters)
  (every #'(lambda (sc)
             (some #'(lambda (cc) (char= sc cc))
                   characters))
         string))

(defparameter *pathname-made-of*
  "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-./")

(defun maybe-update-document! (old-pathname new-pathname
                                new-title new-caption new-author new-attributes)
  (let* ((documents *kura-documents*)
         (satisfy-pathname?
           (and (string-made-of new-pathname *pathname-made-of*)
                (>= 8 (length new-pathname))))
         (doc-nth
           (car (last
                 (loop for doc in documents
                       for index from 0
                       :collect index
                       :while (not (cl-fad:pathname-equal (pathname old-pathname)
                                                          (document-path doc)))))))
         (doc
           (if doc-nth (nth doc-nth documents)))
         (new-attributes (cl-ppcre:split " " new-attributes))
         (tmp-new-doc
           (if doc (make-document :path (pathname new-pathname)
                                  :title new-title :caption new-caption
                                  :dates (document-dates doc)
                                  :author new-author
                                  :attributes new-attributes
                                  :olds (document-olds doc)))))
    (cond ((not satisfy-pathname?)
           (values nil 'nonvalid-pathname))
          ((not doc)
           (values nil 'document-unfound-in-kura-metadatabase))
          ((equalp (nth doc-nth documents) tmp-new-doc)
           (values nil 'not-modified))
          (t
           (progn
             (setf (document-dates tmp-new-doc) (get-universal-time))
             (setf (document-olds tmp-new-doc) (cons (document-dates doc) (document-olds doc)))
             (setf (nth doc-nth documents) tmp-new-doc)
             (cond ((rename-under-kura old-pathname new-pathname) ;; rename
                    (save-structures-to-kura-bases-json-file!)
                    (read-kura-bases-json-file-to-structures!)
                    (values doc nil))
                   (t ;; old-file not found
                    (values nil 'old-file-not-found))))))))




(define-handler-of-librarians (metadatas-editor :uri "/librarian/metas") 
    (old-pathname new-pathname new-attributes new-title new-caption conduct)
  (let ((forming? (string= conduct "forming")))
    (multiple-value-bind (maybe-new-document update-failed)
        (if forming?
            (maybe-update-document! old-pathname new-pathname new-title new-caption
                                    "" new-attributes))
      (let ((forming-row
              (documents-table-for-forming 
               :old-pathname (nil->nil-string old-pathname)
               :new-pathname (nil->nil-string new-pathname)
               :new-attributes (nil->nil-string new-attributes) :new-title (nil->nil-string new-title)
               :new-caption (nil->nil-string new-caption)))
            (documents-table
              (documents-table-for-editing)))
        (funcall
         *tokinowari-template-wide-article*
         :title "目録の管理"
         :content
         (lambda ()
           `(:div :class "article"
             ,@(if forming?
                   `((:h3 "更新に関わる情報")
                     ,(if maybe-new-document
                          `(:p "更新しました" ,maybe-new-document))
                     ,(if update-failed
                          `(:p ,(format nil "更新に失敗しました。 ~A" update-failed)))))
             (:h3 "メタ情報の編集")
             (:div :id "metadata-editor-table" "メタ情報です")
             ,forming-row
             (:h3 "記事の一覧")
             ,documents-table)))))))
  

(in-package #:kage)

;;; server

(defvar *acceptor* nil)



(defun stop-server ()
  (when (and *acceptor* 
             (hunchentoot::started-p *acceptor*))

    (hunchentoot:stop *acceptor*)))

(defun start-server (&key (port 4242))
  "server starting"
  (stop-server)
  (setq hunchentoot:*dispatch-table* nil)
  (hunchentoot:start
   (setf *acceptor* (make-instance 'hunchentoot:easy-acceptor :port port)))
  (asdf:oos 'asdf:load-op :hunchentoot-test)
  (setq hunchentoot:*dispatch-table*
        (append hunchentoot:*dispatch-table*
                (default-tokinoari-dispatch-table))))





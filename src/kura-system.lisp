(in-package #:kage)

;; types

(defun string-intger (string &optional (default nil))
  (if (integerp string) string
      (handler-case
          (parse-integer string)
        (type-error () default)
        (sb-int:simple-parse-error () default))))


(defun datetime-string-to-datetime (string &optional (default 0))
  ;; (datetime-string-to-datetime "2020/12/01 00:20:40 +08")
  (let ((universal
          (handler-case
              (cl-ppcre:register-groups-bind
                  (year month day hour miniute second zone)
                  ("(\\d{4})/(\\d{2})/(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2}) ([+-]\\d{2}|[+-]\\d{1})"
                   string)
                (apply #'encode-universal-time 
                       (mapcar 
                        #'(lambda (v) (string-intger v 1))
                        (list second miniute hour day month year zone))))
            (error () default))))
    (if universal universal default)))

(defparameter *default-time-zone* +9)

(defun datetime-to-datetime-string (datetime &optional (timezone *default-time-zone*))
  (multiple-value-bind
        (second miniute hour day month year)
      (decode-universal-time datetime (- timezone))
    (format nil "~4,'0d/~2,'0d/~2,'0d ~2,'0d:~2,'0d:~2,'0d ~A~A~d"
            year month day
            hour miniute second
            (if (< timezone 0) "-" "+")
            (if (< (abs timezone) 10) "0" "")
            (abs timezone))))



;; struct 

(defstruct (librarian)
  (id "" :type string)
  (name "" :type string)
  (password "" :type string))
  

(defstruct (document)
  (path nil :type pathname)
  (title nil :type string)
  (caption nil :type string)
  (dates nil :type integer) ;; time
  (author nil :type string)
  (attributes nil :type list)
  (olds nil :type list))


;; json kura

(defparameter *kura-documents-bases-json-path*
  (cl-fad:merge-pathnames-as-file
   *kura-documents-bases-dir* #P"documents-metas.json"))


(defun key (key-list)
  (car key-list))

(defun value (key-list)
  (cdr key-list))

(defun cons-lists-documents-to-document-list (cons-lists-documents)
  (remove 
   nil
   (mapcar
    #'(lambda (list-of-document)
        (handler-case
            (let ((path (pathname (value (assoc :path list-of-document))))
                  (title (value (assoc :title list-of-document)))
                  (caption (value (assoc :caption list-of-document)))
                  (dates (datetime-string-to-datetime
                          (format nil "~A" (value (assoc :dates list-of-document)))))
                  (author (value (assoc :author list-of-document)))
                  (attributes (value (assoc :attributes list-of-document)))
                  (olds (value (assoc :olds list-of-document))))
              (make-document
               :path  path :title title :caption caption :dates dates
               :author author :attributes attributes :olds olds))
          (error () nil)))
    cons-lists-documents)))

(defun document-list-to-cons-lists-documents (document-list)
  (mapcar
   #'(lambda (s)
       (mapcar #'(lambda (key val) (cons key (funcall val s)))
               '(:path :title :caption :dates :author :attributes :olds)
               (list #'(lambda (s) (format nil "~A" (document-path s)))
                     #'document-title #'document-caption 
                     #'(lambda (s) (datetime-to-datetime-string (document-dates s)))
                     #'document-author #'document-attributes #'document-olds)))
   document-list))

;; (file-write-date *sample-markdown-file-path*)

(defparameter *last-datetime-kura-file-read* 0)
(defparameter *kura-documents* '())
(defparameter *kura-librarians* '())


(defun read-kura-bases-json-file-to-structures! ()
  (let ((cons-list (cl-json:decode-json-from-string
                    (uiop:read-file-string *kura-documents-bases-json-path*))))
    (setf *kura-librarians*
          (value (assoc :librarians cons-list)))
    (setf *kura-documents*
          (cons-lists-documents-to-document-list
           (value (assoc :documents cons-list))))
    (setf *last-datetime-kura-file-read*
          (get-universal-time))
    (values *kura-librarians* *kura-documents* *last-datetime-kura-file-read*)))



(defun save-structures-to-kura-bases-json-file! ()
  (let* ((cons-list
           (list
            (cons :librarians *kura-librarians*)
            (cons :documents (document-list-to-cons-lists-documents *kura-documents*))))
         (json-string (cl-json:encode-json-alist-to-string cons-list)))
    (write-file json-string *kura-documents-bases-json-path* :action-if-exists :supersede)
    (read-kura-bases-json-file-to-structures!)))




;; file-system

(defun write-file (string outfile &key (action-if-exists :error))
   (check-type action-if-exists (member nil :error :new-version :rename :rename-and-delete 
                                        :overwrite :append :supersede))
   (with-open-file (outstream outfile :direction :output :if-exists action-if-exists)
     (write-sequence string outstream)))


(defun kura-relative-pathname (pathname-related-from-kura)
  (cl-fad:merge-pathnames-as-file *kura-documents-bases-dir* pathname-related-from-kura))

(defun rename-under-kura (old-pathname-related-from-kura new-pathname-related-from-kura)
  (let* ((old-path 
           (kura-relative-pathname old-pathname-related-from-kura))
         (new-path
           (kura-relative-pathname new-pathname-related-from-kura)))
    (handler-case
        (progn (rename-file old-path new-path)
               new-path)
      (error () nil))))


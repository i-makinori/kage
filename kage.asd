
(in-package :cl-user)

(defpackage kage-asd
  (:use :cl :asdf))
(in-package :kage-asd)

(defsystem kage
  :description "kage documents system"
  :version "fai"
  :author "Makinori Ikegami"
  :license "MIT"
  :depends-on (:hunchentoot
               :hunchentoot-test
               :cl-fad
               ;;:cl-markup)
               :inferior-shell
               ;;:cl-yaml
               :cl-json
               :cl-markdown)
  :components ((:file "package")
               (:file "configs")
               (:module "src"
                :components
                ((:module "templates"
                  :components
                  ((:file "html-markup")
                   (:file "tokinowari-template")))
                 (:file "parse-markdown")
                 (:file "kura-system")
                 (:file "routes")
                 (:file "routes-librarians")
                 
                 (:file "server"))))
  )
  
  
  


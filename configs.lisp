(in-package #:kage)

;;; configs


(defparameter *system-root-directory-pathname*
  (cl-fad:pathname-directory-pathname
   ;;(load-time-value
   ;;(or *compile-file-pathname* *load-pathname*))))
   #P"/home/makinori/programs/kage/"))


(defparameter *static-files-directory-pathname*
  (cl-fad:merge-pathnames-as-directory
   *system-root-directory-pathname* #P"domain-common/statics/"))

(defparameter *static-css-files-directory-pathname*
  (cl-fad:merge-pathnames-as-directory
   *static-files-directory-pathname* "css/"))

(defparameter *templates-lisp-html-path*
  nil)

(defparameter *kura-documents-bases-dir*
  (cl-fad:merge-pathnames-as-directory
   *system-root-directory-pathname* #P"kura/"))

(defparameter *kura-documents-bases-path*
  (cl-fad:merge-pathnames-as-file
   *kura-documents-bases-dir* #P"documents-metas.yaml"))

(defparameter *sample-markdown-file-path*
  (cl-fad:merge-pathnames-as-file
   *kura-documents-bases-path* #P"markdown-sample.md"))



